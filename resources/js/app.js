/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import "../sass/chat.sass";
import "../sass/groupchat.scss";

window.Vue = require('vue');
Vue.prototype.EventBus = new Vue();
window.axios = require('axios');
window.VueRouter=require('vue-router').default;

const CreateGroup = Vue.component('creategroup', require('./components/creategroupcomponent.vue').default);
const Chat = Vue.component('chat', require('./components/chatcomponent.vue').default);
const Admin = Vue.component('admin', require('./components/adminchatcomponent.vue').default);
const GroupChat = Vue.component('groupchat', require('./components/groupchatcomponent.vue').default);


Vue.prototype.$baseurl = "https://localhost:8000";


window.router = new VueRouter({
  routes:[
      {
        path: '/',
        name: 'chat',
        component: Chat,
      },
       {
        path: '/admin',
        name: 'admin',
        component: Admin,
      },
      {
        path: '/group',
        name: 'group',
        component: GroupChat,
      },
      {
        path: '/create-group',
        name: 'creategroup',
        component: CreateGroup,
      }
  ],
})
const app = new Vue({
    router,
    el: '#app',
});
